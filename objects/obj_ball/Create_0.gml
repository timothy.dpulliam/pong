/// @desc establish key variables

speed = 3.2;
// Get rid of obnoxious angles
direction = choose(irandom_range(-30, 30), irandom_range(120, 210));
// amount of speed increase each time a paddle hits the ball
delta_speed = 1.05;