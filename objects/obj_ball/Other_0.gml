/// @desc Destroy ball if outside of room

if (hspeed > 0){
	global._player1_score++;	
} else if (hspeed < 0){
	global._player2_score++;
}

global._hits = 0;
instance_destroy();
instance_create_layer(320, 224, "Instances", obj_ball);