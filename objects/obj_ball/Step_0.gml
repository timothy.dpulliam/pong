/// @desc Make sure ball doesn't go outside room

// Keep ball inside the room vertically
ball_top = y + 10;
ball_bottom = y - 10;

if ((ball_top + vspeed) > room_height){
	vspeed *= -1;
} else if((ball_bottom + vspeed) < 0){
	vspeed *= -1;
}

// Horizontal Collision
if (place_meeting(x + hspeed, y, obj_paddle)){
	while(not place_meeting(x + sign(hspeed), y, obj_paddle)){
		x += sign(hspeed);
	}
	// Object's collision boxes are directly next to each outher
	hspeed *= -delta_speed;
	global._hits++;
}

// Vertical Collision
if (place_meeting(x, y + vspeed, obj_paddle)){
	while(not place_meeting(x, y + sign(vspeed), obj_paddle)){
		y += sign(vspeed);
	}
	// Object's collision boxes are directly next to each outher
	vspeed *= -delta_speed;
}

// This needs to be tested
// Basically, the closer to the center of the paddle the ball hits,
// The closer to a 180 degree turn the ball makes
// need to reverse sign if bottom of paddle hits
//direction = direction + 90 + 90 * (point_distance(x, y, other.x, other.y) / (spr_paddle.sprite_height / 2));
