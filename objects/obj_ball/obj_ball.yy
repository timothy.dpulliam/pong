{
    "id": "7910bf98-7370-4450-9e7c-4b7df55f2e78",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ball",
    "eventList": [
        {
            "id": "3820afce-6441-4b3d-94be-7c856025f416",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7910bf98-7370-4450-9e7c-4b7df55f2e78"
        },
        {
            "id": "ce02ce41-cb13-4137-ba64-ec882e4f7ed2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7910bf98-7370-4450-9e7c-4b7df55f2e78"
        },
        {
            "id": "1e76d02e-5905-4c3e-a9e3-82d78cdc66ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7910bf98-7370-4450-9e7c-4b7df55f2e78"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7cc61da2-0aa1-4a77-bc60-3a7042ab315f",
    "visible": true
}