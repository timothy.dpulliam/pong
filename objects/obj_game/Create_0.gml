/// @desc Global Variables

global._player1_score = 0;
global._player2_score = 0;
global._hits = 0;

// Set the random seed
randomize();