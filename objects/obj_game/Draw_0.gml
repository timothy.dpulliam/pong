/// @desc draw player scores

if (room == rm_game){
	draw_text(50, 20, "Player 1 \n" + string(global._player1_score));
	draw_text(400, 20, "Player 2 \n" + string(global._player2_score));
	draw_text(200, 20, "Hits \n" + string(global._hits));
} else if (room == rm_start){
	draw_set_halign(fa_center);
	draw_text_transformed(room_width/2, 200, @"Pong Yourself
Press ENTER to start
Press R to restart game", 2, 2, 0);
	draw_set_halign(fa_left);
}