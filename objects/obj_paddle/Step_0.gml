/// @desc control paddles

// Move the paddles
key_down = keyboard_check(vk_down);
key_up = keyboard_check(vk_up);
top_of_paddle = y - 50;
bottom_of_paddle = y + 50;

if (key_down){
	// Keep the paddles inside the room
	if ((bottom_of_paddle + 5) < room_height){ y += 5; }
} else if (key_up){
	if ((top_of_paddle - 5) > 0){ y -= 5; }
}

// We need to manually detect collision with ball to make sure
// ball doesn't get stuck in the paddle
// If the paddle moves to a place the ball is currently
if (place_meeting(x, y, obj_ball)){
	// kick ball out in the direction it came
	obj_ball.vspeed *= -1;
	obj_ball.y += obj_ball.vspeed + sign(obj_ball.vspeed) * 10;
}