{
    "id": "5945b418-5b02-4326-8060-5bb17fa55ecc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_paddle",
    "eventList": [
        {
            "id": "ab4f90f7-6241-48ef-8f22-731e137b9d9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5945b418-5b02-4326-8060-5bb17fa55ecc"
        },
        {
            "id": "ed3d5463-8f26-490a-89f3-c6eb57fa26f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "5945b418-5b02-4326-8060-5bb17fa55ecc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3556dfc2-49bd-418c-b41c-b0bc77974e77",
    "visible": true
}