{
    "id": "7cc61da2-0aa1-4a77-bc60-3a7042ab315f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "163894b0-a549-4b98-94a5-9ed45ac7a40b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cc61da2-0aa1-4a77-bc60-3a7042ab315f",
            "compositeImage": {
                "id": "bb24984b-6935-4ab6-8a25-633238275b94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163894b0-a549-4b98-94a5-9ed45ac7a40b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96984a34-c9e9-4126-a040-1c85dde60fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163894b0-a549-4b98-94a5-9ed45ac7a40b",
                    "LayerId": "c921fb0f-5a0a-4168-8549-c1ce30d8035d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "c921fb0f-5a0a-4168-8549-c1ce30d8035d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cc61da2-0aa1-4a77-bc60-3a7042ab315f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}