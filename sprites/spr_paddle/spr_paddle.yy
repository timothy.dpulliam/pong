{
    "id": "3556dfc2-49bd-418c-b41c-b0bc77974e77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_paddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3baaaafb-e19e-494c-88fa-bf1512c3bef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3556dfc2-49bd-418c-b41c-b0bc77974e77",
            "compositeImage": {
                "id": "cbf1bb9c-04c6-4f8a-91d5-c265d8044b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3baaaafb-e19e-494c-88fa-bf1512c3bef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72875f2f-8cd3-4f40-a139-7ef4f789ae89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3baaaafb-e19e-494c-88fa-bf1512c3bef5",
                    "LayerId": "f40e2761-6099-41e9-b5aa-6ae2c45db436"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "f40e2761-6099-41e9-b5aa-6ae2c45db436",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3556dfc2-49bd-418c-b41c-b0bc77974e77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 50
}